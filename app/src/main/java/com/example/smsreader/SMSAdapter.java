package com.example.smsreader;


import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SMSAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "SMSAdapter";

    private LayoutInflater inflater;
    private ArrayList<Object> arrayList = new ArrayList<>();
    private Context context;

    private final int SMS = 0;
    private final int TITLE = 1;

    public SMSAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public void setArrayList(ArrayList<Object> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;
        switch (i) {
            case SMS:
                View v1 = inflater.inflate(R.layout.item_sms, viewGroup, false);
                viewHolder = new ViewHolderSMS(v1);
                break;
            case TITLE:
                View v2 = inflater.inflate(R.layout.item_sms_title, viewGroup, false);
                viewHolder = new ViewHolderTitle(v2);
                break;
            default:
                View v3 = inflater.inflate(R.layout.item_sms_title, viewGroup, false);
                viewHolder = new ViewHolderTitle(v3);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case SMS:
                ViewHolderSMS vh1 = (ViewHolderSMS) viewHolder;
                configureViewHolderSMS(vh1, position);
                break;
            case TITLE:
                ViewHolderTitle vh2 = (ViewHolderTitle) viewHolder;
                configureViewHolderTitle(vh2, position);
                break;
            default:
                ViewHolderTitle vh3 = (ViewHolderTitle) viewHolder;
                configureDefault(vh3, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (arrayList.get(position) instanceof SMS) {
            return SMS;
        } else if (arrayList.get(position) instanceof String) {
            return TITLE;
        }
        return -1;
    }

    class ViewHolderSMS extends RecyclerView.ViewHolder {

        @BindView(R.id.sender_name)
        TextView senderName;
        @BindView(R.id.sender_text)
        TextView senderText;
        @BindView(R.id.sender_linear_layout)
        LinearLayout senderLinearLayout;
        @BindView(R.id.sender_card_layout)
        CardView cardView;

        ViewHolderSMS(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ViewHolderTitle extends RecyclerView.ViewHolder {

        @BindView(R.id.title_textview)
        TextView title;

        ViewHolderTitle(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void configureViewHolderSMS(ViewHolderSMS viewHolderSMS, int position) {
        SMS sms = (com.example.smsreader.SMS) arrayList.get(position);
        viewHolderSMS.senderName.setText(sms.getAddress());
        viewHolderSMS.senderText.setText(sms.getMsg());

        if (sms.isBlink()) {
            viewHolderSMS.cardView.setCardBackgroundColor(Color.RED);
        } else {
            viewHolderSMS.cardView.setCardBackgroundColor(Color.WHITE);
        }
    }

    private void configureViewHolderTitle(ViewHolderTitle viewHolderTitle, int position) {
        String title = (String) arrayList.get(position);
        viewHolderTitle.title.setText(title);
        waitDelayMills();
    }

    private void configureDefault(ViewHolderTitle viewHolderTitle, int position) {
        viewHolderTitle.title.setText(context.getString(R.string.error));
    }

    /**
     * Used for the blinking action after a notification is clicked
     */
    private void waitDelayMills() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                for (int i = 0; i < arrayList.size(); i++) {
                    Object object = arrayList.get(i);
                    if (object instanceof SMS) {
                        ((SMS) object).setBlink(false);
                    }
                }
                notifyDataSetChanged();
            }
        }, 1000);

    }

}
