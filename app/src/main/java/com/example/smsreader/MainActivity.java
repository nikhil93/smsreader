package com.example.smsreader;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "MainActivity";

    private static final int SMS_DEFAULT_APP_CODE = 1;
    private static final int CURSOR_LOADER_ID = 12;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.rootLayout)
    LinearLayout linearLayout;

    private SMSAdapter adapter;
    private String _id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new SMSAdapter(this);
        recyclerView.setAdapter(adapter);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(Utils.PENDING_INTENT_ID)) {
            _id = extras.getString(Utils.PENDING_INTENT_ID);
        }

        init();

    }

    private void init() {
        final String myPackageName = getPackageName();
        if (!Telephony.Sms.getDefaultSmsPackage(this).equals(myPackageName)) {
            Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, myPackageName);
            startActivityForResult(intent, SMS_DEFAULT_APP_CODE);
        } else {
            getSupportLoaderManager().initLoader(CURSOR_LOADER_ID, null, this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final String myPackageName = getPackageName();
        if (Telephony.Sms.getDefaultSmsPackage(this).equals(myPackageName)) {
            getSupportLoaderManager().restartLoader(CURSOR_LOADER_ID, null, this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SMS_DEFAULT_APP_CODE) {
            if (resultCode == RESULT_OK) {
                getSupportLoaderManager().initLoader(CURSOR_LOADER_ID, null, this);
            } else {
                Snackbar.make(linearLayout, R.string.default_app_required, Snackbar.LENGTH_INDEFINITE)
                        .setAction("ALLOW", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                init();
                            }
                        }).show();
            }
        }
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        Uri uri = Telephony.Sms.CONTENT_URI;

        Date date = new Date();
        long dayBeforeTime = date.getTime() - 172800000L;
        String dayBeforeString = String.valueOf(dayBeforeTime);

        String[] selectionArgs = {dayBeforeString};
        String selection = " date > ?";

        return new CursorLoader(this
                , uri
                , null
                , selection
                , selectionArgs
                , null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        ArrayList<SMS> list = getSMSList(cursor);
        adapter.setArrayList(sortSMS(list));
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    @SuppressWarnings("unused")
    public void onMessageEvent(MessageEvent event) {
        if (event.refreshData) {
            getSupportLoaderManager().restartLoader(CURSOR_LOADER_ID, null, this);
        }
    }

    /**
     * Converts a cursor to a arrayList of type SMS
     *
     * @param cursor - Database cursor
     * @return - ArrayList of type SMS
     */
    private ArrayList<SMS> getSMSList(Cursor cursor) {
        ArrayList<SMS> smsArrayList = new ArrayList<>();

        if (cursor != null && !cursor.isClosed()) {
            int totalSMS = cursor.getCount();

            if (cursor.moveToFirst()) {
                for (int i = 0; i < totalSMS; i++) {
                    SMS sms = new SMS();
                    String id = cursor.getString(cursor.getColumnIndexOrThrow(Utils.COLUMN_NAME_ID));

                    if (id.equals(_id)) {
                        sms.setBlink(true);
                        _id = "";
                    }
                    sms.setId(id);
                    sms.setAddress(cursor.getString(cursor.getColumnIndexOrThrow(Utils.COLUMN_NAME_ADDRESS)));
                    sms.setMsg(cursor.getString(cursor.getColumnIndexOrThrow(Utils.COLUMN_NAME_BODY)));
                    sms.setReadState(cursor.getString(cursor.getColumnIndex(Utils.COLUMN_NAME_READ)));
                    sms.setTime(cursor.getString(cursor.getColumnIndexOrThrow(Utils.COLUMN_NAME_DATE)));

                    String type = cursor.getString(cursor.getColumnIndexOrThrow(Utils.COLUMN_NAME_TYPE));
                    sms.setType(type);

                    if (type.equals("1")) {
                        smsArrayList.add(sms);
                    }
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        return smsArrayList;
    }

    /**
     * Converts ArrayList of type SMS to ArrayList of type object, this list is used
     * to provide data to the heterogenous recycler view.
     *
     * @param list
     * @return
     */
    private ArrayList<Object> sortSMS(ArrayList<SMS> list) {
        ArrayList<Object> arrayList = new ArrayList<>();
        if (list != null && list.size() > 0) {
            Date date = new Date();
            long time = date.getTime();

            long oneHour = 3600000L;
            long twoHour = 7200000L;
            long threeHour = 10800000L;
            long sixHour = 21600000L;
            long twelveHour = 43200000L;
            long oneDay = 172800000L;

            for (int i = 0; i < list.size(); i++) {

                SMS sms = list.get(i);

                if (Long.parseLong(sms.getTime()) > (time - oneHour)) {
                    if (!arrayList.contains("1 hour")) {
                        arrayList.add("1 hour");
                    }
                    arrayList.add(sms);
                    continue;
                }

                if (Long.parseLong(sms.getTime()) > (time - twoHour)) {
                    if (!arrayList.contains("2 hours")) {
                        arrayList.add("2 hours");
                    }
                    arrayList.add(sms);
                    continue;
                }

                if (Long.parseLong(sms.getTime()) > (time - threeHour)) {
                    if (!arrayList.contains("3 hours")) {
                        arrayList.add("3 hours");
                    }
                    arrayList.add(sms);
                    continue;
                }

                if (Long.parseLong(sms.getTime()) > (time - sixHour)) {
                    if (!arrayList.contains("6 hours")) {
                        arrayList.add("6 hours");
                    }
                    arrayList.add(sms);
                    continue;
                }

                if (Long.parseLong(sms.getTime()) > (time - twelveHour)) {
                    if (!arrayList.contains("12 hours")) {
                        arrayList.add("12 hours");
                    }
                    arrayList.add(sms);
                    continue;
                }

                if (Long.parseLong(sms.getTime()) > (time - oneDay)) {
                    if (!arrayList.contains("1 day")) {
                        arrayList.add("1 day");
                    }
                    arrayList.add(sms);
                }
            }
            return arrayList;
        }
        return arrayList;
    }

}
