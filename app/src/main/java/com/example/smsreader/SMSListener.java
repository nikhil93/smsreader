package com.example.smsreader;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import static com.example.smsreader.Utils.COLUMN_NAME_ADDRESS;
import static com.example.smsreader.Utils.COLUMN_NAME_BODY;
import static com.example.smsreader.Utils.COLUMN_NAME_DATE;
import static com.example.smsreader.Utils.COLUMN_NAME_READ;
import static com.example.smsreader.Utils.COLUMN_NAME_TYPE;
import static com.example.smsreader.Utils.PENDING_INTENT_ID;

public class SMSListener extends BroadcastReceiver {
    private static final String TAG = "SMSListener";

    private String GROUP_KEY_SMS = "com.android.example.SMS";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction() != null &&
                intent.getAction().equals("android.provider.Telephony.SMS_DELIVER")) {

            String smsSender = "";
            String smsSenderDisplayBody = "";
            StringBuilder smsBody = new StringBuilder();
            for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                smsSender = smsMessage.getDisplayOriginatingAddress();
                smsSenderDisplayBody = smsMessage.getDisplayMessageBody();
                smsBody.append(smsMessage.getMessageBody());
            }

            // Writing to the sms content provider since we are the default SMS provider
            Uri uri = writeToContentProvider(context, smsSender, smsBody.toString());

            int currentApiVersion = Build.VERSION.SDK_INT;
            if (currentApiVersion >= Build.VERSION_CODES.O) {
                sendNotificationChannel(context, smsSender, smsBody.toString(), uri);
            } else {
                sendNotificationOld(context, smsSender, smsBody.toString(), uri);
            }

            Log.d(TAG, "SMS detected: From " + smsSender + "\n Body " + smsBody
                    + "\n Display body" + smsSenderDisplayBody);
        }

        EventBus.getDefault().post(new MessageEvent(true));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void sendNotificationChannel(Context context, String title, String body, Uri uri) {
        String name = "SMS";

        String id = uri.getLastPathSegment();

        int notificationId = 1;
        NotificationChannel updateChannel = new NotificationChannel(name
                , "SMS channel"
                , NotificationManager.IMPORTANCE_HIGH);

        updateChannel.setLightColor(Color.GREEN);

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(PENDING_INTENT_ID, id);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(intent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);

        if (notificationManager != null) {
            notificationManager.createNotificationChannel(updateChannel);
            Notification notification = new Notification.Builder(context)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setChannelId(name)
                    .setContentIntent(pendingIntent)
                    .setGroup(GROUP_KEY_SMS)
                    .setAutoCancel(true)
                    .build();

            notificationManager.notify(notificationId, notification);
        }
    }

    private void sendNotificationOld(Context context, String title, String body, Uri uri) {
        String id = uri.getLastPathSegment();

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(PENDING_INTENT_ID, id);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(intent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setGroup(GROUP_KEY_SMS)
                .setContentIntent(pendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body));

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(1, mBuilder.build());
        }
    }

    private Uri writeToContentProvider(Context context, String address, String smsBody) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_ADDRESS, address);
        values.put(COLUMN_NAME_DATE, System.currentTimeMillis() + "");
        values.put(COLUMN_NAME_READ, "1");
        values.put(COLUMN_NAME_TYPE, "1");
        values.put(COLUMN_NAME_BODY, smsBody);
        return context.getContentResolver().insert(Telephony.Sms.CONTENT_URI, values);
    }

}
