package com.example.smsreader;

public class Utils {

    public static final String COLUMN_NAME_ID = "_id";
    public static final String COLUMN_NAME_ADDRESS = "address";
    public static final String COLUMN_NAME_BODY = "body";
    public static final String COLUMN_NAME_READ = "read";
    public static final String COLUMN_NAME_DATE = "date";
    public static final String COLUMN_NAME_TYPE = "type";

    public static final String PENDING_INTENT_ID = "ID";
}
