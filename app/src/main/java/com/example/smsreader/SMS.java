package com.example.smsreader;

public class SMS {


    private String id;
    private String address;
    private String msg;
    private String readState; //"0" for have not read sms and "1" for have read sms
    private String time;
    private String folderName;
    private String type;
    private boolean blink;

    public String getId() {
        return id;
    }

    public void setId(String _id) {
        this.id = _id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String _address) {
        this.address = _address;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String _msg) {
        this.msg = _msg;
    }

    public String getReadState() {
        return readState;
    }

    public void setReadState(String _readState) {
        this.readState = _readState;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String _time) {
        this.time = _time;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String _folderName) {
        this.folderName = _folderName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isBlink() {
        return blink;
    }

    public void setBlink(boolean blink) {
        this.blink = blink;
    }
}
